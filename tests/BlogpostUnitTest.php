<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blogPost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitle('title')
                 ->setCreatedAt($datetime)
                 ->setContent('content')
                 ->setSlug('slug')
                 ->setUser($user);

        $this->assertTrue($blogPost->getTitle() === 'title');
        $this->assertTrue($blogPost->getCreatedAt() === $datetime);
        $this->assertTrue($blogPost->getContent() === 'content');
        $this->assertTrue($blogPost->getSlug() === 'slug');
        $this->assertTrue($blogPost->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $blogPost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitle('titre')
                 ->setCreatedAt($datetime)
                 ->setContent('content')
                 ->setSlug('slug')
                 ->setUser($user);

        $this->assertFalse($blogPost->getTitle() === 'false');
        $this->assertFalse($blogPost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogPost->getContent() === 'false');
        $this->assertFalse($blogPost->getSlug() === 'false');
        $this->assertFalse($blogPost->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $blogPost = new Blogpost();

        $this->assertEmpty($blogPost->getTitle());
        $this->assertEmpty($blogPost->getCreatedAt());
        $this->assertEmpty($blogPost->getContent());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getUser());
        $this->assertEmpty($blogPost->getId());
    }
}
