<?php

namespace App\Tests;

use App\Entity\Category;
use DateTime;
use App\Entity\Paint;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class PaintUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $paint = new Paint();
        $datetime = new DateTime();
        $category = new Category();
        $author = new User();

        $paint->setName('nom')
                 ->setWidth(20.20)
                 ->setHeight(20.20)
                 ->setOnSale(true)
                 ->setCreationDate($datetime)
                 ->setCreatedAt($datetime)
                 ->setDescription('description')
                 ->setPortfolio(true)
                 ->setSlug('slug')
                 ->setFile('file')
                 ->addCategory($category)
                 ->setPrice(20.20)
                 ->setAuthor($author);

        $this->assertTrue($paint->getName() === 'nom');
        $this->assertTrue($paint->getWidth() == 20.20);
        $this->assertTrue($paint->getHeight() == 20.20);
        $this->assertTrue($paint->getOnSale() === true);
        $this->assertTrue($paint->getCreationDate() === $datetime);
        $this->assertTrue($paint->getCreatedAt() === $datetime);
        $this->assertTrue($paint->getDescription() === 'description');
        $this->assertTrue($paint->getPortfolio() === true);
        $this->assertTrue($paint->getSlug() === 'slug');
        $this->assertTrue($paint->getFile() === 'file');
        $this->assertTrue($paint->getPrice() == 20.20);
        $this->assertTrue($paint->getAuthor() === $author);
        $this->assertContains($category, $paint->getCategory());
    }

    public function testIsFalse()
    {
        $paint = new Paint();
        $datetime = new DateTime();
        $category = new Category();
        $author = new User();

        $paint->setName('name')
              ->setWidth(20.20)
              ->setHeight(20.20)
              ->setOnSale(true)
              ->setCreationDate($datetime)
              ->setCreatedAt($datetime)
              ->setDescription('descrption')
              ->setPortfolio(true)
              ->setSlug('slug')
              ->setFile('file')
              ->addCategory($category)
              ->setPrice(20.20)
              ->setAuthor($author);
        
        $this->assertFalse($paint->getName() === 'false');
        $this->assertFalse($paint->getWidth() == 22.20);
        $this->assertFalse($paint->getHeight() == 22.20);
        $this->assertFalse($paint->getOnSale() === false);
        $this->assertFalse($paint->getCreationDate() === new DateTime());
        $this->assertFalse($paint->getCreatedAt() === new DateTime());
        $this->assertFalse($paint->getDescription() === 'false');
        $this->assertFalse($paint->getPortfolio() === false);
        $this->assertFalse($paint->getSlug() === 'false');
        $this->assertFalse($paint->getFile() === 'false');
        $this->assertFalse($paint->getPrice() == 22.20);
        $this->assertFalse($paint->getAuthor() === new User());
        $this->assertNotContains(new Category(), $paint->getCategory());

    }

    public function testIsEmpty()
    {
        $paint = new Paint();

        $paint->setName('')
              ->setWidth('')
              ->setHeight('')
              ->setOnSale('')
              ->setDescription('')
              ->setPortfolio('')
              ->setSlug('')
              ->setFile('')
              ->setPrice('');

              $this->assertEmpty($paint->getName());
              $this->assertEmpty($paint->getWidth());
              $this->assertEmpty($paint->getHeight());
              $this->assertEmpty($paint->getOnSale());
              $this->assertEmpty($paint->getCreationDate());
              $this->assertEmpty($paint->getCreatedAt());
              $this->assertEmpty($paint->getDescription());
              $this->assertEmpty($paint->getPortfolio());
              $this->assertEmpty($paint->getSlug());
              $this->assertEmpty($paint->getFile());
              $this->assertEmpty($paint->getPrice());
              $this->assertEmpty($paint->getCategory());
              $this->assertEmpty($paint->getAuthor());


    }
}
