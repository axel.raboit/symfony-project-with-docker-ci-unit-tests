<?php

namespace App\Tests;

use DateTime;
use App\Entity\Paint;
use App\Entity\Comment;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $comment = new Comment();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Paint();

        $comment->setAuthor('auteur')
                    ->setEmail('email@test.com')
                    ->setCreatedAt($datetime)
                    ->setContent('contenu')
                    ->setBlogpost($blogpost)
                    ->setPaint($peinture);

        $this->assertTrue($comment->getAuthor() === 'auteur');
        $this->assertTrue($comment->getEmail() === 'email@test.com');
        $this->assertTrue($comment->getCreatedAt() === $datetime);
        $this->assertTrue($comment->getContent() === 'contenu');
        $this->assertTrue($comment->getBlogpost() === $blogpost);
        $this->assertTrue($comment->getPaint() === $peinture);

    }

    public function testIsFalse()
    {
        $commentaire = new Comment();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Paint();

        $commentaire->setAuthor('auteur')
                    ->setEmail('email@test.com')
                    ->setCreatedAt($datetime)
                    ->setContent('contenu')
                    ->setBlogpost($blogpost)
                    ->setPaint($peinture);

        $this->assertFalse($commentaire->getAuthor() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getContent() === 'false');
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getPaint() === new Paint());
    }

    public function testIsEmpty()
    {
        $commentaire = new Comment();

        $this->assertEmpty($commentaire->getAuthor());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getContent());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getPaint());
        $this->assertEmpty($commentaire->getId());
    }
}