# Website Symfony avec Gitlab CI / Docker / Unit Tests

Ce site a pour but d'apprendre l'integration d'un site web en utilisant Gitlab CI / Docker / Unit Tests

## Methode de creation du projet

- Creation du projet symfony avec la commande classique
- Creation de la pipeline avec le fichier .gitlab-ci.yml
- Creation de la base de donnée à l'aide de Docker [symfony console make:docker:database]

-> Database en Mysql
-> Ajout automatiquement du fichier docker-compose.yml

## Environement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):

```bash
symfony check:requirements
```

### Lancer l'environement de développement

```bash
docker-composer up -d
symfony serve -d
```

### Commandes utiles pour les tests

- Creation d'un test unitaire
```bash
symfony console make:unit-test
```

- Faire jouer les tests unitaire
```bash
php bin/phpunit --testdox
```